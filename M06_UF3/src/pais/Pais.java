package pais;
import java.util.List;

import cat.iam.ad.uf3.IPais;

public class Pais implements IPais{
	
	private String nom;
	private long longitudFronteres;
	private List<String> grups;
	private String descripcio;
	
	public Pais() {
	
	}

	@Override
	public String getNom() {
		
		return nom;
	}

	@Override
	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public long getLongitudFronteres() {
		return longitudFronteres;
	}

	@Override
	public void setLongitudFronteres(long longitud) {
		longitudFronteres = longitud;
	}

	@Override
	public List<String> getGrupsEtnics() {
		return grups;
	}

	@Override
	public void getGrupsEtnics(List<String> grups) {
		this.grups = grups;
	}

	@Override
	public String creaDescripcio() {
		return descripcio;
	}
	
}
