package pais;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.xquery.XQConnection;
import javax.xml.xquery.XQDataSource;
import javax.xml.xquery.XQException;
import javax.xml.xquery.XQExpression;
import javax.xml.xquery.XQResultItem;
import javax.xml.xquery.XQResultSequence;

import org.basex.api.client.ClientSession;
import org.basex.api.client.Query;
import org.basex.core.BaseXException;
import org.basex.core.Context;
import org.basex.core.cmd.Close;
import org.basex.core.cmd.CreateDB;
import org.basex.core.cmd.Open;
import org.basex.core.cmd.XQuery;

import net.xqj.basex.BaseXXQDataSource;

import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextArea;
import javax.swing.JMenu;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Principal extends JFrame implements ActionListener {

	private JPanel contentPane;
	private static XQuery query;
	private static String resposta;
	private static long resposta2;
	private static List<String> resposta3;
	private JTextArea textArea;
	private static JComboBox combo;
	private JRadioButtonMenuItem radioButtonMenuItem;
	private JRadioButtonMenuItem rdbtnmntmBasex;
	private JRadioButtonMenuItem rdbtnmntmXqj;
	private JButton btnGeneraPais;
	private JButton btnNewButton;
	private boolean inc = false;
	private boolean baseX = false;
	private boolean xqj = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 129, 21);
		contentPane.add(menuBar);

		JMenu mnNewMenu = new JMenu("Mode");
		menuBar.add(mnNewMenu);

		// Conexions
		// Mode Incrustat
		radioButtonMenuItem = new JRadioButtonMenuItem("Incrustat");
		radioButtonMenuItem.setSelected(true);
		mnNewMenu.add(radioButtonMenuItem);
		radioButtonMenuItem.addActionListener(this);

		// Mode ServerBaseX
		rdbtnmntmBasex = new JRadioButtonMenuItem("BaseX");
		mnNewMenu.add(rdbtnmntmBasex);
		rdbtnmntmBasex.addActionListener(this);

		// Mode ServerXQJ
		rdbtnmntmXqj = new JRadioButtonMenuItem("XQJ");
		mnNewMenu.add(rdbtnmntmXqj);
		rdbtnmntmXqj.addActionListener(this);

		combo = new JComboBox();
		combo.setBounds(12, 51, 173, 21);
		contentPane.add(combo);

		textArea = new JTextArea();
		textArea.setBounds(22, 84, 186, 133);
		contentPane.add(textArea);
		textArea.setColumns(10);

		// boto genera html
		btnNewButton = new JButton("Genera HTML");
		btnNewButton.setBounds(246, 125, 157, 31);
		contentPane.add(btnNewButton);
		btnNewButton.addActionListener(this);

		// boto genera pais
		btnGeneraPais = new JButton("Genera pais");
		btnGeneraPais.setBounds(246, 90, 157, 31);
		contentPane.add(btnGeneraPais);
		btnGeneraPais.addActionListener(this);

		incrustat();

	}

	// conexio del mode incrustat
	public void incrustat() {
		Context context = new Context();

		try {
			new Open("fb").execute(context);
		} catch (BaseXException e) {
			try {
				new CreateDB("fb", "factbook.xml").execute(context);
			} catch (BaseXException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		try {
			String preg = "for $pais in //country return $pais/name/text()";
			XQuery query = new XQuery(preg);
			resposta = query.execute(context);
			for (String p : resposta.split("\n")) {
				combo.addItem(p);
			}
			new Close().execute(context);
		} catch (BaseXException e) {
			e.printStackTrace();
		}
		context.close();
	}

	// conexio del mode ServerBaseX
	public void ServerBaseX() {
		try {
			ClientSession session = new ClientSession("localhost", 1984, "admin", "admin");
			// creem la base de dades
			session.execute("CREATE DATABASE fb /media/Iruela/dam2/DAM2/workspace/M06_UF3/factbook.xml");

			String resposta = session.execute("XQUERY for $pais in doc('fb')//country "
					+ "return $pais/name/text()");
			combo.removeAllItems();
			for (String p : resposta.split("\n")) {
				combo.addItem(p);
			}
			session.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	// conexio del mode Server
	public void ServerXQJ() {
		XQDataSource source = new BaseXXQDataSource();
		
		String pregunta = "for $pais in doc('fb')//country return $pais/name/text()";
		try{
			source.setProperty("serverName", "localhost");
			source.setProperty("port", "1984");
			source.setProperty("user", "admin");
			source.setProperty("password", "admin");
			
			XQConnection connection = source.getConnection();
			
			XQExpression expression = connection.createExpression();
			XQResultSequence seq = expression.executeQuery(pregunta);
			
			combo.removeAllItems();
			while (seq.next()) {
				String nomPais = seq.getItemAsString(null);
				combo.addItem(nomPais);
			}
		}catch(XQException e){
			e.printStackTrace();
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() instanceof JRadioButtonMenuItem) {
			JRadioButtonMenuItem instance = (JRadioButtonMenuItem) e.getSource();
			// si mode incrustat
			if (instance.getText().equals("Incrustat")) {
				incrustat();
				radioButtonMenuItem.setSelected(true);
				rdbtnmntmBasex.setSelected(false);
				rdbtnmntmXqj.setSelected(false);
				inc = true;
			} else if (instance.getText().equals("BaseX")) {
				// si mode ServerBaseX
				ServerBaseX();
				rdbtnmntmBasex.setSelected(true);
				radioButtonMenuItem.setSelected(false);
				rdbtnmntmXqj.setSelected(false);
				baseX = true;
			} else {
				// si mode ServerXQJ
				ServerXQJ();
				rdbtnmntmXqj.setSelected(true);
				rdbtnmntmBasex.setSelected(false);
				radioButtonMenuItem.setSelected(false);
				xqj = true;
			}
		}

		if (e.getSource() instanceof JButton) {
			// Action Listener boto geenera pais
			JButton instance = (JButton) e.getSource();
			// si button genera pais mode incrustat
			if (instance.getText().equals("Genera pais") && inc) {
				Context context = new Context();
				try {
					new Open("fb").execute(context);
				} catch (BaseXException e1) {
					e1.printStackTrace();
				}
				Pais p = new Pais(); // objecte pais

				// consulta nom
				String nom = "for $pais in //country " + "return $pais/name[text() = '" + combo.getSelectedItem()
						+ "']/text()";
				query = new XQuery(nom);
				try {
					resposta = query.execute(context);
				} catch (BaseXException e1) {
					e1.printStackTrace();
				}
				p.setNom(resposta); // afegim nom al objecte pais

				// consulta suma longituts de les seves fronteres
				String longitud = "for $pais in //country[name/text() = '" + combo.getSelectedItem() + "'] "
						+ "let $total := sum($pais//border/@length/data()) " + "return $total";
				query = new XQuery(longitud);
				try {
					resposta = query.execute(context);
				} catch (BaseXException e1) {
					e1.printStackTrace();
				}
				resposta2 = (long) Double.parseDouble(resposta);
				p.setLongitudFronteres(resposta2); // afegim longitud al objecte
													// pais

				// consulata grup etnics
				String grupsE = "for $pais in //country[name/text() = '" + combo.getSelectedItem() + "']"
						+ "return $pais//ethnicgroups/text()";
				query = new XQuery(grupsE);
				try {
					resposta = query.execute(context);
				} catch (BaseXException e1) {
					e1.printStackTrace();
				}
				resposta3 = new ArrayList<String>(); // creem array dels grups
														// etnics
				resposta3.add(resposta); // afegim cada grup a l'array
				p.getGrupsEtnics(resposta3); // afegim grups etnics al objecte
												// pais

				// toString de l'objecte pais
				String toString = "Nom: " + p.getNom() + "\nLongitud: " + p.getLongitudFronteres() + "\nGrups Etnics: "
						+ p.getGrupsEtnics();

				// afegim al text area el toString
				textArea.setText(toString);
				context.close();
			}
			// baseX
			else if (instance.getText().equals("Genera pais") && baseX) {
				Pais p = new Pais();
				try {
					ClientSession session = new ClientSession("localhost", 1984, "admin", "admin");
				
				// consulta nom
				String nom = session.execute("XQUERY for $pais in doc('fb')//country " + "return $pais/name[text() = '" + combo.getSelectedItem()
						+ "']/text()");
				p.setNom(nom);
				
				// consulta suma longituts de les seves fronteres
				String longitud = session.execute("XQUERY for $pais in doc('fb')//country[name/text() = '" + combo.getSelectedItem() + "'] "
						+ "let $total := sum($pais//border/@length/data()) " + "return $total");
				
				resposta2 = (long) Double.parseDouble(longitud);
				p.setLongitudFronteres(resposta2);
				
				// consulata grup etnics
				String grupsE = session.execute("XQUERY for $pais in doc('fb')//country[name/text() = '" + combo.getSelectedItem() + "']"
						+ "return $pais//ethnicgroups/text()");
				
				resposta3 = new ArrayList<String>();
				resposta3.add(grupsE);
				p.getGrupsEtnics(resposta3);
				
				// toString de l'objecte pais
				String toString = "Nom: " + p.getNom() + "\nLongitud: " + p.getLongitudFronteres() + "\nGrups Etnics: "
						+ p.getGrupsEtnics();

				// afegim al text area el toString
				textArea.setText(toString);
				
				session.close();
				
				}catch(IOException e4){
					e4.printStackTrace();
				}
				
			}
			// xqj
			else if (instance.getText().equals("Genera pais") && xqj) {

			}
			// si button genera HTML incrustat
			else if (instance.getText().equals("Genera HTML") && inc) {

			} 
			//baseX
			else if (instance.getText().equals("Genera HTML") && baseX) {

			} 
			//xqj
			else if (instance.getText().equals("Genera HTML") && xqj) {

			}
		}
	}
}
